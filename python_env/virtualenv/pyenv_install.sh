#!/bin/bash
## 通过脚本程序安装pyenv环境
## 针对每个用户安装pyenv环境

# 设置命令的环境变量
PATH=${PATH}:/usr/bin
NOTINSTALL=9

# 设置系统环境包
# centos
# 如果安装失败需要额外安装这些包
# sudo yum -y groupinstall "Development tools"
# sudo yum -y install zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel gdbm-devel db4-devel libpcap-devel xz-devel
# sudo yum install libffi-devel -y

# 1. 获取pyenv包
which git &> /dev/null || {
    echo "git not INSTALLed"
    echo "Please install git"
    exit ${NOTINSTALL}
}
# 2. 配置 pyenv
echo "从github拉取安装包中..."
git clone https://github.com/yyuu/pyenv.git ~/.pyenv && {
    echo "添加配置pyenv配置中..."
    echo "# pyenv settings" >> ~/.bash_profile
    echo 'export PYENV_ROOT="${HOME}/.pyenv"' >> ~/.bash_profile
    echo 'export PATH="${PYENV_ROOT}/bin:$PATH"' >> ~/.bash_profile
    echo 'eval "$(pyenv init -)"' >> ~/.bash_profile
    /bin/bash ~/.bash_profile
} && {
        echo "配置成功"
} || {
        echo "配置失败"
}


# 3. 验证安装
echo $(pyenv --version) || {
    echo "Install or Setting pyenv env Error"
    exit 2
}

## 安装virtualenv
echo "Starting Install pyenv-virtualenv"
git clone https://github.com/yyuu/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv && {
        echo "Set the pyenv-virtualenv"
        echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.bash_profile
        /bin/bash ~/.bash_profile
} || {
        echo "配置pyenv-virtualenv失败"
        exit 2
}

# 校验安装
echo "Checking Install"
pyenv virtualenvs || {
        echo "安装失败"
}
