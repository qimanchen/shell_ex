#!/bin/bash
#Author:chen qiman
#Time:2020-09-10 00:15:48
#Name:create_file_share.sh
#Version:V1.0
#Description: create on http service for share file in special dirctory

#############
# arguments
############
DIR=${1:-`pwd`}
NOTPYTHON=200

##########################
# check python is install
##########################


########################
# check python versions
########################

PYTHONVERSION=`python --version|awk '{print $2}'|awk -F"." '{print $1}'`
if [ ${PYTHONVERSION} -eq 2 ];then
    python -m SimpleHTTPServer &
elif [ ${PYTHONVERSION} -eq 3 ];then
    python -m http.server &
else
    echo "python NOT install"
    exit $NOTPYTHON
fi

