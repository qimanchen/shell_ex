"""
批量更新gitbook中指定css和js链接
"""
import os
import fnmatch

# old_css = '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">'
# old_js = '<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>'

old_css = '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">'
old_js = '<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>'

new_css = '<link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css">'
new_js = '<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>'

def is_file_match(filename, patterns):
    """
    找出指定格式的文件
    """
    for pattern in patterns:
        if fnmatch.fnmatch(filename, pattern):
            return True
    return False


def find_specific_files(root, patterns=["*"], exclude_dirs=[]):
    """
    遍历文件系统
    """
    for root, dirnames, filenames in os.walk(root):
        for filename in filenames:
            if is_file_match(filename, patterns):
                yield os.path.join(root, filename)
        for d in exclude_dirs:
            if d in dirnames:
                dirnames.remove(d)


def replace_file(path):
    """
    替换指定文件
    """
    root = path
    patterns = ["*.html"]
    for f in find_specific_files(root, patterns):
        with open(f,"a+", encoding="utf8") as tmp_f:
            tmp_f.seek(0)
            content = tmp_f.read()
            print("="*20)
            print("更新文件: ", f)
            content = content.replace(old_css, new_css, 1)
            content = content.replace(old_js, new_js,1)
            # print(content)
            # print(content.replace())
            print("="*20)
        with open(f,"w", encoding="utf8") as tmp_f:
            tmp_f.write(content)
        


if __name__ == "__main__":
    path = "测试文件"
    replace_file(path)

