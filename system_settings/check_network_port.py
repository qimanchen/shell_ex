#!/usr/bin/env python
#-*- coding: utf-8 -*-
#Author:chen qiman
#Time:2020-10-21 16:09:48
#Name:check_network_port.py
#Version:V1.0
"""
侦测某个ip下的端口是否开通
"""
from __future__ import print_function
import socket
import sys

def get_all_host_ip():
    """获取主机的所有ip(127.0.0.1)除外"""
    addrs = socket.getaddrinfo(socket.gethostname(), None)
    ips = set()
    for item in addrs:
        ip = item[-1][0]
        if ":" not in ip:
            ips.add(ip)
    return list(ips)

def get_host_ip():
    """获得本机第一个网卡的ip"""
    hostname = socket.gethostname()
    return socket.gethostbyname(hostname)


def conn_scan(host, port):
    """扫描主机开启的端口"""
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        client_socket.connect((host, port))
        print("{}:{} is open".format(host, port))
    except Exception:
        #print(f"{host}:{port} is closed")
        pass
    finally:
        client_socket.close()


# @click.command()
# @click.option("--ip", "-H", default="192.168.90.253", help="ip")
# @click.option("--ports", "-p", default="65535", help="port ranges")
# @click.option("--timeout", "-t", default=2, help="connect timeout")
def check_tcp_port(ports=65535):
    """检测主机上所有IP开启接口的情况"""
    for ip in get_all_host_ip():
        print("============ {} =============".format(ip))
        for port in range(1, ports+1):
            conn_scan(ip, port)


if __name__ == "__main__":
    check_tcp_port()
    # get_all_host_ip()
    
